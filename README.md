# LIFENOTE WPF

An application of Time Tracking, created with C# and WPF.

## How it work?

Its really a simple application like an Agenda, you have the calendar and the 
task of each day that are programatically updated each time you change day
on the calendar.

I have try to add some other fonctions like change colour theme of the application
or send and email of the resume of your task, but infortunally the extra fonctions 
are still not finished so they have some little bug. (have only set the gmail
for send email for example).

DONT WORRY about the extra fonctionnality of this application, the main goal is
to be able to create, update and delete task and this is working well.


### Interface

Login window

![alt text](Screenshot/LoginScreen.PNG "Login window screenshot")

Main window

![alt text](Screenshot/MainScreen.PNG "Main window screenshot")



## Evolutions

1) Need to find a way to have different users in the same application

2) Need to set more differents language in the settings

3) Each user have is own settins of theme colours, language of the whole application
and type of email (gmail, hotmail.etc..)