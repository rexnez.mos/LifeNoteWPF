﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LifenoteData.Entity
{
    public partial class Objective
    {
        //CONSTRUCTEUR
        public Objective()
        { }
        public Objective(Objective _o)
        {
            this.IdObjective = _o.IdObjective;
            this.Description = _o.Description;
            this.IdListTask = _o.IdListTask;
            this.ListTask = _o.ListTask;
            this.LimitDay = _o.LimitDay;
        }
        //VARIABLES
        private bool _isnewO = false;
        //PROPRIETES
        public bool IsNewObjective
        {
            get { return _isnewO; }
            set { _isnewO = value; }
        }
        //METHODES
        public static Objective GetNewObjective(Objective tempO)
        {
            Objective o = new Objective(tempO);
            o.IdObjective = -1;
            o.IsNewObjective = true;
            return o;
        }
    }
}
