﻿using LifenoteData.Entity;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LifenoteData.Entity
{
    public partial class ListTask
    {
        public ListTask(ListTask lt)
        {
            IdListTask = lt.IdListTask;
            DateOfList = lt.DateOfList;
            IdWork = lt.IdWork;
            Work = lt.Work;
            Notes = lt.Notes;
            Objectives = lt.Objectives;
        }
        public static ListTask GetNewListTask(DateTime _dateLT)
        {
            ListTask lt = new ListTask();
            lt.IdListTask = -1;
            lt.DateOfList = _dateLT;
            lt.IsNew = true;
            return lt;
        }
        private bool _isnew = false;
        public bool IsNew
        {
            get { return _isnew; }
            set { _isnew = value; }
        }

    }
}

