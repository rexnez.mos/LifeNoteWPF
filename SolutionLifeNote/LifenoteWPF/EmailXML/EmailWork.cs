﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LifenoteWPF.EmailXML
{
    public class EmailWork
    {
        public EmailWork()
        { }
        public EmailWork(DateTime date, TimeSpan start, TimeSpan end, Nullable<System.TimeSpan> pauseStart, Nullable<System.TimeSpan> pauseEnd) : this()
        {
            DateWork = date;
            StartWork = start;
            EndWork = end;
            PauseStart = pauseStart;
            PauseEnd = pauseEnd;
        }
        public TimeSpan StartWork { get; set; }
        public TimeSpan EndWork { get; set; }
        public Nullable<System.TimeSpan> PauseStart { get; set; }
        public Nullable<System.TimeSpan> PauseEnd { get; set; }
        public DateTime DateWork { get; set; }
    }
}
