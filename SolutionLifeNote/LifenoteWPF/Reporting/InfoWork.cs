﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LifenoteWPF.Reporting
{
    public class InfoWork
    {
        public InfoWork()
        { }
        public InfoWork(int id, DateTime date, TimeSpan start, TimeSpan end, TimeSpan tot, TimeSpan real) : this()
        {
            IDwork = id;
            DateWork = date;
            StartWork = start;
            EndWork = end;
            TotTime = tot;
            RealTime = real;
        }
        public InfoWork(int _id, string _description ) :this()
        {
            IDwork = _id;
            Description = _description;
        }
        public TimeSpan StartWork { get; set; }
        public TimeSpan EndWork { get; set; }
        public TimeSpan TotTime { get; set; }
        public TimeSpan RealTime { get; set; }
        public int IDwork { get; set; }
        public string Description { get; set; }
        public DateTime DateWork { get; set; }
    }
}
