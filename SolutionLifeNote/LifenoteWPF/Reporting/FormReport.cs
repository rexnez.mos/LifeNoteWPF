﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LifenoteWPF.Reporting
{
    public partial class FormReport : Form
    {
        public FormReport()
        {
            InitializeComponent();
        }

        private void FormReport_Load(object sender, EventArgs e)
        {
            this.reportViewer1.RefreshReport();
        }
        public FormReport(WorkReporting wr) : this()
        {
            this.WorkReportingBindingSource.DataSource = wr;
            this.reportViewer1.RefreshReport();
        }

        private void reportViewer1_Load(object sender, EventArgs e)
        {

            
        }

        private void WorkReportingBindingSource_CurrentChanged(object sender, EventArgs e)
        {

        }
    }
}
