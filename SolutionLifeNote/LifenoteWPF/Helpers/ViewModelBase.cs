﻿using LifenoteData.Entity;
using LifenoteWPF.View;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;

namespace LifenoteWPF.Helpers
{
    /// <summary>
    /// Base class for all ViewModel classes in the application.
    /// It provides support for property change notifications 
    /// and has a DisplayName property.  This class is abstract.
    /// </summary>
    public abstract class ViewModelBase : INotifyPropertyChanged, IDisposable
    {
        #region Personalisation

        ////// CONSTRUCTEUR
        protected ViewModelBase()
        {
            ctx = new LifeNoteSQLdataEntities();
            if (((MainWindow)System.Windows.Application.Current.MainWindow) != null)
                MyInfoUser = ((MainWindow)System.Windows.Application.Current.MainWindow).InfoForUser;
        }

        ////// VARIABLES
        protected LifeNoteSQLdataEntities ctx;
        protected ListTask _currentListTask;
        protected ObservableCollection<ListTask> _ListTasks;
        protected ObservableCollection<Note> _listNotes;
        protected ObservableCollection<Work> _listWork;
        protected ObservableCollection<Objective> _listObjectives;
        protected ObservableCollection<Alarm> _listAlarms;
        protected Work _currentWork;
        protected System.Windows.Controls.Label MyInfoUser;
        private Brush _black = new SolidColorBrush(Colors.Black);
        private Brush _white = new SolidColorBrush(Colors.White);

        ////// COMMAND
        protected RelayCommand _newCommandWork;
        protected RelayCommand _newCommandNote;
        protected RelayCommand _newCommandObjective;
        protected RelayCommand _printCommand;
        protected RelayCommand _saveCommand;
        protected RelayCommand _deleteCommand;
        protected RelayCommand _annulerCommand;
        protected RelayCommand _emailCommand;
        public void ChangeBackgroundInfo()
        {
            if ((MyInfoUser.Background.ToString()) == (White.ToString()))
            {
                MyInfoUser.Background = new SolidColorBrush(System.Windows.Media.Colors.Black);
            }
            else
            {
                MyInfoUser.Background = new SolidColorBrush(System.Windows.Media.Colors.White);
            }
        }
        /////// PROPRIETES GET/SET
        public ObservableCollection<ListTask> ListTasks
        {
            get { return _ListTasks; }
            set
            {
                _ListTasks.Clear();
                _ListTasks = value;
                FirePropertyChanged();
            }
        }
        public ListTask CurrentListTask
        {
            get
            {
                return _currentListTask;
            }
            set
            {
                _currentListTask = value;
                FirePropertyChanged();
            }
        }
        public Work CurrentWork
        {
            get
            {
                return _currentWork;
            }
            set
            {
                _currentWork = value;
                FirePropertyChanged();
            }
        }
        public ObservableCollection<Work> ListWork
        {
            get { return _listWork; }
            set
            {
                _listWork = value;
                FirePropertyChanged();
            }
        }
        public ObservableCollection<Objective> ListObjectives
        {
            get { return _listObjectives; }
            set
            {
                _listObjectives = value;
                FirePropertyChanged();
            }
        }
        public ObservableCollection<Note> ListNotes
        {
            get { return _listNotes; }
            set
            {
                _listNotes = value;
                FirePropertyChanged();
            }
        }
        public ObservableCollection<Alarm> ListAlarms
        {
            get { return _listAlarms; }
            set
            {
                _listAlarms = value;
                FirePropertyChanged();
            }
        }
        public Nullable<System.TimeSpan> InfoTimeAlarm { get; set; }
        public DateTime InfoDateTask { get; set; }
        public Brush Black
        {
            get
            {
                return _black;
            }

            set
            {
                _black = value;
                FirePropertyChanged();
            }
        }
        public Brush White
        {
            get
            {
                return _white;
            }

            set
            {
                _white = value;
                FirePropertyChanged();
            }
        }

        //////////////////////////////
        #endregion Personalisation

        #region DisplayName

        /// <summary>
        /// Returns the user-friendly name of this object.
        /// Child classes can set this property to a new value,
        /// or override it to determine the value on-demand.
        /// </summary>
        public virtual string DisplayName { get; protected set; }

        #endregion // DisplayName

        #region Debugging Aides

        /// <summary>
        /// Warns the developer if this object does not have
        /// a public property with the specified name. This 
        /// method does not exist in a Release build.
        /// </summary>
        [Conditional("DEBUG")]
        [DebuggerStepThrough]
        public void VerifyPropertyName(string propertyName)
        {
            // Verify that the property name matches a real,  
            // public, instance property on this object.
            if (TypeDescriptor.GetProperties(this)[propertyName] == null)
            {
                string msg = "Invalid property name: " + propertyName;

                if (this.ThrowOnInvalidPropertyName)
                    throw new Exception(msg);
                else
                    Debug.Fail(msg);
            }
        }

        /// <summary>
        /// Returns whether an exception is thrown, or if a Debug.Fail() is used
        /// when an invalid property name is passed to the VerifyPropertyName method.
        /// The default value is false, but subclasses used by unit tests might 
        /// override this property's getter to return true.
        /// </summary>
        protected virtual bool ThrowOnInvalidPropertyName { get; private set; }

        #endregion // Debugging Aides

        #region INotifyPropertyChanged Members

        /// <summary>
        /// Raised when a property on this object has a new value.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Raises this object's PropertyChanged event.
        /// </summary>
        /// <param name="propertyName">The property that has a new value.</param>
        public void FirePropertyChanged([CallerMemberName] string propertyName = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }

        }

        #endregion // INotifyPropertyChanged Members

        #region IDisposable Members

        /// <summary>
        /// Invoked when this object is being removed from the application
        /// and will be subject to garbage collection.
        /// </summary>
        public void Dispose()
        {
            this.OnDispose();
        }

        /// <summary>
        /// Child classes can override this method to perform 
        /// clean-up logic, such as removing event handlers.
        /// </summary>
        protected virtual void OnDispose()
        {
        }

#if DEBUG
        /// <summary>
        /// Useful for ensuring that ViewModel objects are properly garbage collected.
        /// </summary>
        ~ViewModelBase()
        {
            string msg = string.Format("{0} ({1}) ({2}) Finalized", this.GetType().Name, this.DisplayName, this.GetHashCode());
            System.Diagnostics.Debug.WriteLine(msg);
        }
#endif

        #endregion // IDisposable Members
    }
}