﻿using LifenoteData.Entity;
using LifenoteWPF.Helpers;
using LifenoteWPF.View;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace LifenoteWPF.ViewModel
{
    public class WorkViewModel : ViewModelBase
    {
        //CONSTRUCTEUR
        public WorkViewModel()
        { }
        public WorkViewModel(ListTask lt) : this()
        {
            if (lt.IsNew)
            {
                _currentListTask = lt;
                _currentWork = new Work() { IsNew = true, TimeStart = new System.TimeSpan(0, 0, 0), TimeEnd = new System.TimeSpan(0, 0, 0) };
            }
            else
            {
                //si c'est une modif alors j'ai crée l'instance depuis la base des données pouir recuperé la list
                _ListTasks = new ObservableCollection<ListTask>(ctx.ListTasks);

                //dans cette liste ont controle le quelle a choisé par le user
                for (int i = 0; i < _ListTasks.Count; i++)
                {
                    //ont filtre par idlisttask
                    if (_ListTasks[i].IdListTask == lt.IdListTask)
                    {
                        //et ont prenne les valeurs
                        _currentListTask = _ListTasks[i];
                        if (_currentListTask.Work == null)
                            _currentWork = new Work() { IsNew = true, TimeStart = new System.TimeSpan(0, 0, 0), TimeEnd = new System.TimeSpan(0, 0, 0) };
                        else
                            _currentWork = _currentListTask.Work;
                    }
                }
                if (_currentWork.Alarm != null)
                    InfoTimeAlarm = _currentWork.Alarm.TimeAlarm;
                _datecopy = _currentListTask.DateOfList;
                _copyLT = new ListTask(_currentListTask);
                _copyWork = new Work(_currentWork);
            }

        }
        //VARIABLES
        private DateTime _datecopy = new DateTime();
        private ListTask _copyLT = new ListTask();
        private Work _copyWork = new Work();
        private bool needcopy = false;
        bool foundAlarm = false;
        private bool _idLTok = false;
        private bool datewaschanged = false;
        
        //PROPRIETES
        public Alarm CurrentAlarmWork { get; set; }

        //COMMAND
        public ICommand SaveCommand
        {
            get
            {
                if (_saveCommand == null)
                {
                    _saveCommand = new RelayCommand(param => this.Save());
                }
                return _saveCommand;
            }
        }
        public ICommand DeleteCommand
        {
            get
            {
                if (_deleteCommand == null)
                {
                    _deleteCommand = new RelayCommand(
                        param => this.DeleteWork(param));
                }
                return _deleteCommand;
            }
        }
        public ICommand AnnulerCommand
        {
            get
            {
                if (_annulerCommand == null)
                {
                    _annulerCommand = new RelayCommand(param => this.Annuler());
                }
                return _annulerCommand;
            }
        }
        //METHODES
        public void Save()
        {
            bool _flag = false;
            if (!_currentWork.IsNew && _datecopy != _currentListTask.DateOfList)
            {
                MessageBoxResult result = MessageBox.Show("You have change the day of the Work, would you keep also the orignal Work?", "Alert!", MessageBoxButton.YesNoCancel);
                switch (result)
                {
                    case MessageBoxResult.Yes:
                        needcopy = true;
                        break;
                    case MessageBoxResult.No:
                        break;
                    case MessageBoxResult.Cancel:
                        _flag = true;
                        break;
                }
                datewaschanged = true;
            }
            if (!_flag)
            {
                if (InfoTimeAlarm != null) //check if alarm is empty if yes = delete
                {
                    foreach (Alarm a in ctx.Alarms) //check if already another alarm existe
                    {
                        if (InfoTimeAlarm == a.TimeAlarm)
                        {
                            _currentWork.IdAlarm = a.IdAlarm;
                            foundAlarm = true;
                        }
                    }
                    if (!foundAlarm)
                    {
                        _currentWork.Alarm = new Alarm() { IsNewAlarm = true, TimeAlarm = new System.TimeSpan(0, 0, 0) };
                        _currentWork.Alarm.TimeAlarm = TimeSpan.Parse((InfoTimeAlarm.ToString()));
                    }
                }
                foreach (ListTask iLT in ctx.ListTasks)
                {
                    if (iLT.DateOfList == _currentListTask.DateOfList)
                    {
                        if (iLT.IdWork == null)
                        {
                            _currentWork = Work.GetNewWork(_currentWork);
                            ObservableCollection<ListTask> tempLT = new ObservableCollection<ListTask>();
                            tempLT.Add(iLT);
                            _currentWork.ListTasks = tempLT;
                        }
                        else
                        {
                            if (!(iLT.IdWork == _currentListTask.IdWork))
                            {
                                MessageBoxResult result = MessageBox.Show("There is already another work on the same day, you want to overwriting it?", "Alert!", MessageBoxButton.YesNo);
                                switch (result)
                                {
                                    case MessageBoxResult.Yes:
                                        iLT.Work = _currentWork; ;
                                        iLT.IdWork = _currentWork.IdWork;
                                        _currentWork.IsNew = false;
                                        break;
                                    case MessageBoxResult.No:
                                        MyInfoUser.Content = "Please change the day";
                                        ChangeBackgroundInfo();
                                        _flag = true;
                                        break;
                                }
                            }
                        }
                    }
                    if (_flag)
                        break;
                }

                if (_currentWork.IsNew && !_flag) //check if user click new
                {
                    foreach (ListTask lt in ctx.ListTasks)  //check list from context
                    {
                        if (lt.DateOfList == _currentListTask.DateOfList)  //check if date already exist
                        {
                            _currentListTask.IdListTask = lt.IdListTask;
                            _idLTok = true;
                        }
                    }
                    if (!_idLTok)      //if didnt find a date a new one is created with id= m-1
                        _currentListTask = ListTask.GetNewListTask(_currentListTask.DateOfList);
                    _currentWork = Work.GetNewWork(_currentWork);   //also for Work id= -1
                    _currentListTask.IdWork = _currentWork.IdWork;  //write the idwork on the CurrentlListTask 
                                                                    //check if already another alarm existe
                    _currentListTask.Work = _currentWork;
                    //add to context
                    ctx.Works.Add(_currentWork);
                    if (!_idLTok)
                        ctx.ListTasks.Add(_currentListTask);
                    if (!foundAlarm)
                        if (InfoTimeAlarm != null)
                            ctx.Alarms.Add(_currentWork.Alarm);
                }

                if (datewaschanged && !_flag)
                {
                    _idLTok = false;
                    _currentWork.ListTasks.Clear();
                    _currentListTask.IdWork = null;
                    _currentListTask.Work = null;
                    ListTask newtempLT = new ListTask() { DateOfList = _currentListTask.DateOfList };
                    _currentListTask.DateOfList = _datecopy;
                    //try to find id for list task
                    foreach (ListTask ltask in ctx.ListTasks)
                    {
                        if (ltask.DateOfList == _datecopy)
                        {
                            ltask.IdWork = null;
                            ltask.Work = null;
                        }
                        if (ltask.DateOfList == newtempLT.DateOfList)
                        {
                            newtempLT = ltask;
                            _idLTok = true;
                        }
                    }
                    if (!_idLTok)      //if didnt find a date a new one is created with id= m-1
                        newtempLT.IdListTask = -1;
                    //connect list task to work
                    ObservableCollection<ListTask> templt = new ObservableCollection<ListTask>();
                    newtempLT.Work = _currentWork;
                    newtempLT.IdWork = _currentWork.IdWork;
                    templt.Add(newtempLT);
                    _currentWork.ListTasks = new ObservableCollection<ListTask>(templt);
                    if (!_idLTok)
                        ctx.ListTasks.Add(newtempLT);

                }
                if (!_flag)
                {
                    try
                    {
                        ctx.SaveChanges();
                        MyInfoUser.Content = "You Saved in the Database";
                        ChangeBackgroundInfo();
                    }
                    catch (Exception e)
                    {
                        System.Windows.MessageBox.Show(e.ToString());
                    }
                    if (needcopy)
                    {
                        //need to cancel info
                        _copyWork.IdWork = -1;
                        _copyWork.ListTasks.Clear();
                        _copyLT.IdWork = null;
                        _copyLT.Work = null;
                        //find old info
                        ObservableCollection<ListTask> listcopyLT = new ObservableCollection<ListTask>();
                        foreach (ListTask ltask in ctx.ListTasks)
                            if (ltask.DateOfList == _copyLT.DateOfList)
                                _copyLT = ltask;

                        listcopyLT.Add(_copyLT);
                        _copyWork.ListTasks = new ObservableCollection<ListTask>(listcopyLT);
                        _copyLT.Work = _copyWork;
                        ctx.Works.Add(_copyWork);
                        ctx.SaveChanges();
                    }
                    RefreshInfo();
                }
            }

        }
        public void DeleteWork(object param)
        {
            bool flag = false;
            MessageBoxResult result = MessageBox.Show("You really want to delete?", "Alert!", MessageBoxButton.YesNo);
            switch (result)
            {
                case MessageBoxResult.Yes:
                    break;
                case MessageBoxResult.No:
                    flag = true;
                    break;
            }
            if (!flag)
            {
                param = _currentWork;
                if (_currentWork.IsNew)
                {
                    _currentWork = null;
                    _currentListTask = null;
                    InfoTimeAlarm = null;
                }
                else
                {
                    foreach (ListTask lt in ctx.ListTasks)
                    {
                        if (lt.IdWork == _currentWork.IdWork)
                        {
                            lt.IdWork = null;
                            ctx.Works.Remove(lt.Work);
                        }
                    }
                    ctx.Works.Remove(_currentWork);
                    foreach (Alarm a in ctx.Alarms)
                    {
                        foreach (Work _w in a.Works)
                        {
                            if (_w.IdWork == _currentWork.IdWork && _w.Alarm != null)
                                a.Works.Remove(_w);
                        }
                    }
                }
                try
                {
                    ctx.SaveChanges();
                    RefreshInfo();
                    MyInfoUser.Content = "Deleted succesfully";
                    ChangeBackgroundInfo();
                }
                catch (Exception e)
                {
                    System.Windows.MessageBox.Show(e.ToString());
                }
            }
        }
        public void Annuler()
        {
            if (MessageBox.Show("Are you sure?", "Annuler", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.No)
            {
                return;
            }
            else
            {
                RefreshInfo();
            }
        }
        public void RefreshInfo()
        {
            ((MainWindow)System.Windows.Application.Current.MainWindow).stackpanelDetailsWork.Children.Clear();
            ((MainWindow)System.Windows.Application.Current.MainWindow).stackUCEvent.Children.Clear();
            ((MainWindow)System.Windows.Application.Current.MainWindow).Refresh();
            ((MainWindow)System.Windows.Application.Current.MainWindow).TabCalendar.IsSelected = true;
        }
    }
}