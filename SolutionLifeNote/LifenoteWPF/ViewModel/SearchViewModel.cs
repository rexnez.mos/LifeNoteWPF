﻿using LifenoteData.Entity;
using LifenoteWPF.Helpers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LifenoteWPF.ViewModel
{
    public class SearchViewModel : ViewModelBase
    {
        //VARIABLES
        private ObservableCollection<ListTask> _searchListTask;
        private ObservableCollection<Work> _searchListWork;
        private ObservableCollection<Note> _searchListNote;
        private ObservableCollection<Objective> _searchListObjective;
        private Work _searchWork;
        private Note _searchNote;
        private Objective _searchObjective;
        private DateTime _todaySearch = DateTime.Today;
        private string _searchBoxDescription;
        private int _workfound;
        private int _notefound;
        private int _objectivefound;
        private Nullable<DateTime> _dateDebutSearch;
        private Nullable<DateTime> _dateFinSearch;
        //PROPRIETES
        public ObservableCollection<ListTask> SearchListTask
        {
            get { return _searchListTask; }
            set
            {
                _searchListTask = value;
                FirePropertyChanged();
            }
        }
        public ObservableCollection<Work> SearchListWork
        {
            get
            {
                return _searchListWork;
            }
            set
            {
                _searchListWork = value;
                FirePropertyChanged();
            }
        }
        public ObservableCollection<Objective> SearchListObjective
        {
            get { return _searchListObjective; }
            set
            {
                _searchListObjective = value;
                FirePropertyChanged();

            }
        }
        public ObservableCollection<Note> SearchListNote
        {
            get { return _searchListNote; }
            set
            {
                _searchListNote = value;
                FirePropertyChanged();

            }
        }
        public Nullable<DateTime> DateDebutSearch
        {
            get {
                if(_dateDebutSearch == null)
                    _dateDebutSearch = new DateTime(_todaySearch.Year, _todaySearch.Month, 1);
                return _dateDebutSearch; }
            set
            {
                _dateDebutSearch = value;
            }
        }
        public Nullable<DateTime> DateFinSearch
        {
            get {
                if (_dateFinSearch == null)
                    _dateFinSearch = new DateTime(_todaySearch.Year, _todaySearch.Month, DateTime.DaysInMonth(_todaySearch.Year, _todaySearch.Month));
                return _dateFinSearch; }
            set
            {
                _dateFinSearch = value;
            }
        }
        public Work SearchWork
        {
            get { return _searchWork; }
            set
            {
                _searchWork = value;
                FirePropertyChanged();
            }
        }
        public Note SearchNote
        {
            get { return _searchNote; }
            set
            {
                _searchNote = value;
                FirePropertyChanged();
            }
        }
        public Objective SearchObjective
        {
            get { return _searchObjective; }
            set
            {
                _searchObjective = value;
                FirePropertyChanged();
            }
        }
        public string SearchBoxDescription
        {
            get {if (_searchBoxDescription == null)
                    _searchBoxDescription = "here";
                return _searchBoxDescription; }
            set { _searchBoxDescription = value;
                FirePropertyChanged();
            }
        }
        public string WorkFound
        {
            get {
                string temp = String.Format("{0} WORK found", _workfound);
                return temp; }
        }
        public string NoteFound
        {
            get
            {
                string temp = String.Format("{0} NOTE found", _notefound);
                return temp;
            }
        }
        public string ObjectiveFound
        {
            get
            {
                string temp = String.Format("{0} OBJECTIVE found", _objectivefound);
                return temp;
            }
        }
        //CONSTRUTECUTER
        public SearchViewModel()
        { }
        public SearchViewModel(string descriptionToFind, Nullable<DateTime> dateDebutToFind, Nullable<DateTime> dateFinToFind)
        {
            _searchBoxDescription = descriptionToFind;
            _dateDebutSearch = dateDebutToFind;
            _dateFinSearch = dateFinToFind;
            //instace de liste pour le search 
            SearchListTask = new ObservableCollection<ListTask>(ctx.ListTasks);
            SearchListWork = new ObservableCollection<Work>();
            SearchListObjective = new ObservableCollection<Objective>();
            SearchListNote = new ObservableCollection<Note>();
            foreach (ListTask lt in _searchListTask)
            {
                ListTask temp = new ListTask();
                temp = lt;
                //liste des works
                if(lt.IdWork.HasValue)
                    if (lt.Work.Description == _searchBoxDescription)
                        if (lt.DateOfList >= DateDebutSearch && lt.DateOfList <= DateFinSearch)
                            _searchListWork.Add(new Work(lt.Work));
                //liste des notes
                foreach (Note n in lt.Notes)
                    if (n.Description == _searchBoxDescription)
                        if (lt.DateOfList >= DateDebutSearch && lt.DateOfList <= DateFinSearch)
                            _searchListNote.Add(new Note(n));
                //liste des objectives
                foreach (Objective o in lt.Objectives)
                    if (o.Description == _searchBoxDescription)
                        if (lt.DateOfList >= DateDebutSearch && lt.DateOfList <= DateFinSearch)
                            _searchListObjective.Add(new Objective(o));
            }
            if (SearchListWork.Count > 0)
                this._searchWork = this.SearchListWork[0];
            if (SearchListNote.Count > 0)
                this._searchNote = this.SearchListNote[0];
            if (SearchListObjective.Count > 0)
                this._searchObjective = this.SearchListObjective[0];
            _workfound = _searchListWork.Count();
            _notefound = _searchListNote.Count();
            _objectivefound = _searchListObjective.Count();
        }
    }
}