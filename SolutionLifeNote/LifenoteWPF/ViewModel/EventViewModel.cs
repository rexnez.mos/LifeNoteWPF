﻿using LifenoteData.Entity;
using LifenoteWPF.EmailXML;
using LifenoteWPF.Helpers;
using LifenoteWPF.Reporting;
using LifenoteWPF.View;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace LifenoteWPF.ViewModel
{
    public class EventViewModel : ViewModelBase
    {
        private DateTime _day;
        //CONSTRUCTERUR PRINCIPAL
        public EventViewModel()
        { }
        public EventViewModel(DateTime daySelected) : this()
        {
            Init(daySelected);
        }
        private void Init(DateTime _daySelected)
        {
            _day = _daySelected;
            //instance de la list qui point sur le context
            _ListTasks = new ObservableCollection<ListTask>(ctx.ListTasks.
                Where(lt => lt.DateOfList == _daySelected));
            //si aucune tache prevue pour la date selectionné une nouvelle vas etre crée
            if (_ListTasks.Count == 0)
            {
                _currentListTask = new ListTask() { IsNew = true, DateOfList = _daySelected };
                _currentWork = new Work() { IsNew = true };
            }
            //sinon a ce date il y aurait 1 seul element ListTask
            else
                _currentListTask = _ListTasks[0];
            //a cette element correspond un Work
            _currentWork = _currentListTask.Work;
            _listWork = new ObservableCollection<Work>();
            if (_currentWork != null)
                _listWork.Add(new Work(_currentWork));
            
            //ainsi que un liste des Notes
            _listNotes = new ObservableCollection<Note>(ctx.Notes.Where(no => no.ListTask.DateOfList == _daySelected));

            _listObjectives = new ObservableCollection<Objective>(ctx.Objectives.
                Where(ob => ob.LimitDay >= _daySelected && ob.ListTask.DateOfList <= _daySelected));
            //ajout un liste pour toutes les alarmes
            _listAlarms = new ObservableCollection<Alarm>(ctx.Alarms);
            //si est un jour du weekend la proprieté dans Work vas etre modifié
            if (_daySelected.DayOfWeek == DayOfWeek.Saturday || _daySelected.DayOfWeek == DayOfWeek.Sunday)
                if (_currentWork != null)
                    _currentWork.IsWeekend = true;
        }
        //COMMAND
        public ICommand PrintCommand
        {
            get
            {
                if (_printCommand == null)
                {
                    _printCommand = new RelayCommand(param => this.Print());
                }
                return _printCommand;
            }
        }
        public ICommand EmailCommand  
        { 
            get
            {
                if (_emailCommand == null)
                {
                    _emailCommand = new RelayCommand(param => this.Email());
                }
                return _emailCommand;
            }
        }
        public ICommand NewCommandWork
        {
            get
            {
                if (_newCommandWork == null)
                {
                    _newCommandWork = new RelayCommand(
                        param => this.NewWork());
                }
                return _newCommandWork;
            }
        }
        public ICommand NewCommandNote
        {
            get
            {
                if (_newCommandNote == null)
                {
                    _newCommandNote = new RelayCommand(
                        param => this.NewNote());
                }
                return _newCommandNote;
            }
        }
        public ICommand NewCommandObjective
        {
            get
            {
                if (_newCommandObjective == null)
                {
                    _newCommandObjective = new RelayCommand(
                        param => this.NewObjective());
                }
                return _newCommandObjective;
            }
        }
        //METHODES
        public void NewWork()
        {
            ListTask lt = new ListTask() { IsNew = true, DateOfList = _day };
            UserControlWork ucw = new UserControlWork(lt);

            ((MainWindow)System.Windows.Application.Current.MainWindow).stackpanelDetailsWork.Children.Clear();
            ((MainWindow)System.Windows.Application.Current.MainWindow).stackpanelDetailsWork.Children.Add(ucw);
            ((MainWindow)System.Windows.Application.Current.MainWindow).TabW.IsSelected = true;
        }
        public void NewNote()
        {
            Note n = new Note() { IsNewNote = true, TimeStart = new TimeSpan(0, 0, 0), TimeEnd = new TimeSpan(0, 0, 0) };
            ListTask lt = new ListTask() { IsNew = true, DateOfList = _day };
            n.ListTask = lt;
            UserControlNote ucn = new UserControlNote(n);

            ((MainWindow)System.Windows.Application.Current.MainWindow).stackpanelDetailsNote.Children.Clear();
            ((MainWindow)System.Windows.Application.Current.MainWindow).stackpanelDetailsNote.Children.Add(ucn);
            ((MainWindow)System.Windows.Application.Current.MainWindow).TabN.IsSelected = true;
        }
        public void NewObjective()
        {
            Objective o = new Objective() { IsNewObjective = true, LimitDay = DateTime.Today };
            ListTask lt = new ListTask() { IsNew = true, DateOfList = _day };
            o.ListTask = lt;
            UserControlObjective uco = new UserControlObjective(o);

            ((MainWindow)System.Windows.Application.Current.MainWindow).stackpanelDetailsOBJ.Children.Clear();
            ((MainWindow)System.Windows.Application.Current.MainWindow).stackpanelDetailsOBJ.Children.Add(uco);
            ((MainWindow)System.Windows.Application.Current.MainWindow).TabO.IsSelected = true;
        }
        public void Print()
        {
            List<InfoWork> listinfowork = new List<InfoWork>();
            ObservableCollection<ListTask> listLT = new ObservableCollection<ListTask>();
            InfoWork _printWork = new InfoWork();

            foreach (ListTask lt in ctx.ListTasks)
            {
                if (lt.Work != null)
                {
                    if (lt.DateOfList.DayOfWeek == DayOfWeek.Saturday || lt.DateOfList.DayOfWeek == DayOfWeek.Sunday)
                        lt.Work.IsWeekend = true;
                    _printWork = new InfoWork(lt.Work.IdWork, lt.DateOfList, lt.Work.TimeStart, lt.Work.TimeEnd
                                               , lt.Work.TotHeures, lt.Work.HeuresEffective);
                    listinfowork.Add(_printWork);
                }
            }
            SelectDatePrint sdp = new SelectDatePrint(listinfowork);
            sdp.ShowDialog();
        }
        public void Email()
        {
            List<EmailWork> listinfowork = new List<EmailWork>();
            ObservableCollection<ListTask> listLT = new ObservableCollection<ListTask>();
            EmailWork _emailWork = new EmailWork();
            foreach (ListTask lt in ctx.ListTasks)
            {
                if (lt.Work != null)
                {
                    _emailWork = new EmailWork(lt.DateOfList,lt.Work.TimeStart,lt.Work.TimeEnd,lt.Work.PauseStart,lt.Work.PauseEnd);
                    listinfowork.Add(_emailWork);
                }
            }
            InfoEmail infoEmail = new InfoEmail(listinfowork);
            infoEmail.ShowDialog();
        }
    }
}
