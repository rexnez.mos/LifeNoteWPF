﻿using LifenoteData.Entity;
using LifenoteWPF.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LifenoteWPF.View
{
    /// <summary>
    /// Logique d'interaction pour UserControlSearch.xaml
    /// </summary>
    public partial class UserControlSearch : UserControl
    {
        public UserControlSearch()
        {
            InitializeComponent();
            DataContext = new SearchViewModel(txtSearchBoxDescription.Text, DPfrom.SelectedDate, DPto.SelectedDate);
        }

        private void ComboWork_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ComboWork.SelectedItem == null) return;
            ListTask lt = null;
            var b = (Work)ComboWork.SelectedItem;
            if (b != null)
            {
                ObservableCollection<ListTask> _listSlt = new ObservableCollection<ListTask>(b.ListTasks);
                lt = _listSlt[0];
            }
            UserControlWork ucw = new UserControlWork(lt);
            ((MainWindow)System.Windows.Application.Current.MainWindow).stackpanelDetailsWork.Children.Clear();
            ((MainWindow)System.Windows.Application.Current.MainWindow).stackpanelDetailsWork.Children.Add(ucw);
            ComboWork.SelectedItem = null;
            ((MainWindow)System.Windows.Application.Current.MainWindow).TabW.IsSelected = true;
        }

        private void ComboNote_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ComboNote.SelectedItem == null) return;
            Note n = null;
            var b = (Note)ComboNote.SelectedItem;
            if (b != null)
            {
                n = b;
            }
            UserControlNote ucn = new UserControlNote(n);

            ((MainWindow)System.Windows.Application.Current.MainWindow).stackpanelDetailsNote.Children.Clear();
            ((MainWindow)System.Windows.Application.Current.MainWindow).stackpanelDetailsNote.Children.Add(ucn);
            ComboNote.SelectedItem = null;
            ((MainWindow)System.Windows.Application.Current.MainWindow).TabN.IsSelected = true;
        }

        private void ComboObjective_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
            if (ComboObjective.SelectedItem == null) return;
            Objective ob = null;
            var b = (Objective)ComboObjective.SelectedItem;
            if (b != null)
            {
                ob = b;
            }
            UserControlObjective ucn = new UserControlObjective(ob);

            ((MainWindow)System.Windows.Application.Current.MainWindow).stackpanelDetailsOBJ.Children.Clear();
            ((MainWindow)System.Windows.Application.Current.MainWindow).stackpanelDetailsOBJ.Children.Add(ucn);
            ComboObjective.SelectedItem = null;
            ((MainWindow)System.Windows.Application.Current.MainWindow).TabO.IsSelected = true;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DataContext = new SearchViewModel(txtSearchBoxDescription.Text, DPfrom.SelectedDate, DPto.SelectedDate);
        }
    }
}
