﻿using LifenoteData.Entity;
using LifenoteWPF.View;
using System;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using MahApps.Metro.Controls;

namespace LifenoteWPF.View
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        //COSTRUCTEUR
        public MainWindow()
        {
            LoadLogin();

            InitializeComponent();
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            LoadToday();
        }
        //VARIABLES
        public DateTime _today;
        protected DateTime _dayselected;
        private UserControl ucEvent;
        private UserControl ucSearch;
        //METHODES DE BASE
        private void LoadLogin()
        {
            LoginWindow lw = new LoginWindow();
            this.Hide();
            lw.ShowDialog();

            if (lw.DialogResult.HasValue & lw.DialogResult.Value)
            {
                lw.Close();
                this.Show();
            }
            else
                this.Close();

        }
        private void LoadToday()
        {
            _today = DateTime.Today;
            MyCalendar.SelectedDate = _today;
            if (ucEvent == null)
                ucEvent = new UserControlEvent(_today);
            stackUCEvent.Children.Clear();
            stackUCEvent.Children.Add(ucEvent);
            TODAYvalue.Text = _today.ToString("ddd dd/MM/yyyy");
        }
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show(this, "You really want to exit?", "Question", MessageBoxButton.YesNo, MessageBoxImage.Warning);

            if (result == MessageBoxResult.No)
            {
                e.Cancel = true;
            }
        }
        public void Refresh()
        {
            ucEvent = null;
            ucSearch = null;

            if (ucEvent == null)
                ucEvent = new UserControlEvent(_dayselected);
            stackUCEvent.Children.Clear();
            stackUCEvent.Children.Add(ucEvent);

            if (ucSearch == null)
                ucSearch = new UserControlSearch();
            stackUCSearch.Children.Clear();
            stackUCSearch.Children.Add(ucSearch);
        }
        //CLICK BUTTON
        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
        //METHODE CALENDRIER
        private void MyCalendar_OnSelectedDatesChanged(object sender, SelectionChangedEventArgs e)
        {
            ucEvent = null;

            SelectedDateTextBox.Text = MyCalendar.SelectedDate.ToString();

            if (SelectedDateTextBox.Text != null)
                SelectedDateTextBox.Text = MyCalendar.SelectedDate.Value.ToString("dd/MM/yyyy");

            if (_dayselected != MyCalendar.SelectedDate.Value)
                _dayselected = MyCalendar.SelectedDate.Value;

            if (ucEvent == null)
                ucEvent = new UserControlEvent(_dayselected);

            stackUCEvent.Children.Clear();
            stackUCEvent.Children.Add(ucEvent);
        }
        private MetroWindow themeSettingsWindow;
        private void Button_Settings_Click(object sender, RoutedEventArgs e)
        {
            if (themeSettingsWindow != null)
            {
                themeSettingsWindow.Activate();
                return;
            }

            themeSettingsWindow = new Settings();
            themeSettingsWindow.Owner = this;
            themeSettingsWindow.Closed += (o, args) => themeSettingsWindow = null;
            themeSettingsWindow.Left = this.Left + this.ActualWidth / 2.0;
            themeSettingsWindow.Top = this.Top + this.ActualHeight / 2.0;
            themeSettingsWindow.Show();
        }
    }
}