﻿using LifenoteData.Entity;
using LifenoteWPF.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LifenoteWPF.View
{
    /// <summary>
    /// Logique d'interaction pour UserControlObjective.xaml
    /// </summary>
    public partial class UserControlObjective : UserControl
    {
        public UserControlObjective()
        {
            InitializeComponent();
        }
        public UserControlObjective(Objective ob) : this()
        {
            DataContext = new ObjectiveViewModel(ob);
        }
    }
}
