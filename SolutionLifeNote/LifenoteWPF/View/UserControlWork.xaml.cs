﻿using LifenoteWPF.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using LifenoteWPF.View;
using System.Windows.Input;
using LifenoteData.Entity;
using System.Windows.Markup;
using System.Globalization;

namespace LifenoteWPF.View
{
    /// <summary>
    /// Logique d'interaction pour UserControlWork.xaml
    /// </summary>
    public partial class UserControlWork : UserControl
    {
        public UserControlWork()
        {
            InitializeComponent();
        }
        public UserControlWork(ListTask lt) : this()
        {
            DataContext = new WorkViewModel(lt);
        }
        private string timeEndOldSender = "00:00";
        private string timeStartOldSender = "00:00";
        private string pauseEndOldSender = "00:00";
        private string pauseStartOldSender = "00:00";
        private string timeAlarmOldSender = "00:00";
        private bool CheckTime(string TestThisTime)
        {
            int htime = 0;
            int mtime = 0;
            string[] TimeSplitted = TestThisTime.Split(':');
            bool okParseH = int.TryParse(TimeSplitted[0], out htime);
            bool okParseM = int.TryParse(TimeSplitted[1], out mtime);
            if (okParseH && okParseM)
            {
                if (htime > 23 || mtime > 59)
                    okParseH = false;
            }
            else
                okParseH = false;
            return okParseH;

        }

        private void txtALARM_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox textBox = sender as TextBox;
            if (txtAlarm.Text != "")
            {
                if (CheckTime(txtAlarm.Text))
                    timeAlarmOldSender = textBox.Text;
                else
                    textBox.Text = timeAlarmOldSender;
            }
        }

        private void txtTimeEnd_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox textBox = sender as TextBox;

            if (CheckTime(txtTimeEnd.Text))
                timeEndOldSender = textBox.Text;
            else
                textBox.Text = timeEndOldSender;
        }
        private void txtTimeStart_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox textBox = sender as TextBox;

            if (CheckTime(txtTimeStart.Text))
                timeStartOldSender = textBox.Text;
            else
                textBox.Text = timeStartOldSender;
        }
        private void txtPauseStart_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox textBox = sender as TextBox;
            if (PauseStart.Text != "")
            {
                if (CheckTime(PauseStart.Text))
                    pauseStartOldSender = textBox.Text;
                else
                    textBox.Text = pauseStartOldSender;
            }
        }
        private void txtPauseEnd_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox textBox = sender as TextBox;
            if (PauseEnd.Text != "")
            {
                if (CheckTime(PauseEnd.Text))
                    pauseEndOldSender = textBox.Text;
                else
                    textBox.Text = pauseEndOldSender;
            }
        }

        private void Time_GotFocus(object sender, System.Windows.RoutedEventArgs e)
        {
            TextBox textBox = sender as TextBox;
            if (textBox.Text == "")
            {
                textBox.Text = "00:00";
            }
        }
    }
}
