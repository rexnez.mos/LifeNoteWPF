﻿using LifenoteData.Entity;
using LifenoteWPF.Helpers;
using LifenoteWPF.ViewModel;
using System.Windows.Controls;
using System.Windows.Input;

namespace LifenoteWPF.View
{
    /// <summary>
    /// Logique d'interaction pour UcWorkDay.xaml
    /// </summary>
    public partial class UcWorkDay : UserControl
    {
        public UcWorkDay()
        {
            InitializeComponent();
        }

        private void Selected_Work(object sender, MouseButtonEventArgs e)
        {
            EventViewModel evm = ((EventViewModel)DataContext);

            ListTask lt = evm.CurrentListTask;

            Work w = null;

            if (madatagridWORK.SelectedItem == null) return;

            w = (madatagridWORK.SelectedItem as Work);

            lt.Work = w;

            UserControlWork ucw = new UserControlWork(lt);

            ((MainWindow)System.Windows.Application.Current.MainWindow).stackpanelDetailsWork.Children.Clear();
            ((MainWindow)System.Windows.Application.Current.MainWindow).stackpanelDetailsWork.Children.Add(ucw);
            ((MainWindow)System.Windows.Application.Current.MainWindow).TabW.IsSelected = true;
        }

    }
}
